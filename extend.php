<?php

/*
 * This file is part of ffzhou/flarum-rich-text-extend.
 *
 * Copyright (c) 2021 Zhouweijie.
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Ffzhou\RichTextExtend;

use Flarum\Extend;

return [
    (new Extend\Frontend('forum'))
        ->js(__DIR__.'/js/dist/forum.js'),
        
    
    
];
