# Rich Text Extend

![License](https://img.shields.io/badge/license-MIT-blue.svg) [![Latest Stable Version](https://img.shields.io/packagist/v/ffzhou/flarum-rich-text-extend.svg)](https://packagist.org/packages/ffzhou/flarum-rich-text-extend)

A [Flarum](http://flarum.org) extension. Rich text extension, the BBCode format image into markdown format

### Installation

Use [Bazaar](https://discuss.flarum.org/d/5151-flagrow-bazaar-the-extension-marketplace) or install manually with composer:

```sh
composer require ffzhou/flarum-rich-text-extend
```

### Updating

```sh
composer update ffzhou/flarum-rich-text-extend
```

### Links

- [Packagist](https://packagist.org/packages/ffzhou/flarum-rich-text-extend)
